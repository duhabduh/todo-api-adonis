import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Statuses extends BaseSchema {
  protected tableName = 'statuses'

  public async up() {
    this.schema.table(this.tableName, (table) => {
      table.integer('project_id')
        .unsigned()
        .references('id')
        .inTable('projects')
    })
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {
      table.dropForeign(['project_id'])
      table.dropColumn('project_id')
    })
  }
}
