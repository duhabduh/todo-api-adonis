import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'
import UserValidator from 'App/Validators/UserValidator'


export default class AuthController {
  public async login({ request, auth, response }: HttpContextContract) {
    const email = request.input('email')
    const password = request.input('password')

    const token = await auth.use('api').attempt(email, password)
    return response.status(200).json({ message: 'login success', token })
  }

  /**
   * 
   * @swagger  
   * /api/v1/register:
   *  post:
   *    tags: 
   *      - Authentication
   *    requestBody:
   *      required: true
   *      content:
   *        application/x-www-form-urlencoded:
   *          schema:
   *            $ref: '#definitions/User'
   *        application/json:
   *          schema:
   *            $ref: '#definitions/User'
   *    responses:
   *      201:
   *        description: user created, verify otp in email
   *      422:
   *        description: request invalid
   */
  public async register({ request, response }: HttpContextContract) {
    try {
      const data = await request.validate(UserValidator)

      const newUser = await User.create(data)

      const otp_code = Math.floor(100000 + Math.random() * 900000)

      await Database.table('otp_codes').insert({ otp_code: otp_code, user_id: newUser.id })

      await Mail.send((message) => {
        message
          .from('admin@todoapi.com')
          .to(data.email)
          .subject('Welcome Onboard!')
          .htmlView('emails/otp_verification', { otp_code })
      })

      return response.created({ message: 'register success, please verify your otp code' })
    } catch (error) {
      response.status(422).json({ message: 'Unprocessable Entity', errors: error.messages.errors })
    }

  }

  public async otpConfirmation({ request, response }: HttpContextContract) {

    try {
      let otp_code = request.input('otp_code')
      let email = request.input('email')

      let user = await User.findByOrFail('email', email)
      let otpCheck = await Database.query().from('otp_codes').where('otp_code', otp_code).first()

      if (user?.id == otpCheck.user_id) {
        user.isVerified = true
        await user?.save()
        return response.status(200).json({ message: 'berhasil konfirmasi OTP' })
      } else {
        throw Error("gagal konfirmasi OTP")
      }
    } catch (error) {
      return response.status(400).json({ error })
    }
  }
}
